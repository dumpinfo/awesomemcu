LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
ENTITY shijia IS
    PORT (CLK,RST,EN : IN STD_LOGIC;                     
          CQ : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);     
          COUT : OUT STD_LOGIC  );           
END ;
ARCHITECTURE behav OF shijia IS
BEGIN
   PROCESS(CLK, RST, EN)
     VARIABLE  CQI : STD_LOGIC_VECTOR(3 DOWNTO 0); 
   BEGIN
      IF RST = '1' THEN   CQI := (OTHERS =>'0') ;  --计数器复位          
      ELSIF CLK'EVENT AND CLK='1' THEN        --检测时钟上升沿
        IF EN = '1' THEN                      --检测是否允许计数
          IF CQI < "1001" THEN   CQI := CQI + 1; --允许计数  
            ELSE    CQI := (OTHERS =>'0');--大于9，计数值清零       
          END IF;
        END IF;
      END IF;   
       IF CQI = "1001" THEN COUT <= '1'; --计数大于9，输出进位信号
         ELSE    COUT <= '0';
       END IF;
         CQ <= CQI;                           --将计数值向端口输出
   END PROCESS;
END ;
