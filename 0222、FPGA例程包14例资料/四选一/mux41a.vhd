ENTITY mux41a IS
  PORT (a,b,c,d:IN BIT;
            s,g:IN BIT;
              y:OUT BIT);
END ENTITY mux41a;
ARCHITECTURE one OF mux41a IS
  BEGIN
 PROCESS(s,g,a,b,c,d)
BEGIN
    IF s='0' THEN
    IF g='0' THEN y<=a;
       ELSE y<=b; 
    END IF;
   ELSIF s='1' THEN
     IF  g='0' THEN y<=c; 
       ELSE y<=d; 
     END IF;
  END IF;
END PROCESS;
END ARCHITECTURE one;