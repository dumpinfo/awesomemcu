 LIBRARY IEEE ;
 USE IEEE.STD_LOGIC_1164.ALL ;
 ENTITY led IS
  PORT ( A  : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
      led: OUT STD_LOGIC_VECTOR(6 DOWNTO 0)  ) ;
 END ;
 ARCHITECTURE one OF led IS
 BEGIN
  PROCESS( A )
  BEGIN
  CASE  A  IS
   WHEN "0000" =>  led <=  "0111111" ; 
   WHEN "0001" =>  led  <= "0000110" ; 
   WHEN "0010" =>  led  <= "1011011" ; 
   WHEN "0011" =>  led  <= "1001111" ; 
   WHEN "0100" =>  led  <= "1100110" ; 
   WHEN "0101" =>  led  <= "1101101" ; 
   WHEN "0110" =>  led  <= "1111101" ;
   WHEN "0111" =>  led  <= "0000111" ;
   WHEN "1000" =>  led  <= "1111111" ; 
   WHEN "1001" =>  led  <= "1101111" ;
   WHEN "1010" =>  led  <= "1110111" ;
   WHEN "1011" =>  led  <= "1111100" ;
   WHEN "1100" =>  led  <= "0111001" ; 
   WHEN "1101" =>  led  <= "1011110" ;
   WHEN "1110" =>  led  <= "1111001" ; 
   WHEN "1111" =>  led  <= "1110001" ;
   WHEN OTHERS =>  NULL ;
   END CASE ;
  END PROCESS ;
 END ;