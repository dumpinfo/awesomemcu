library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY bcd IS
   PORT (
      clk                     : IN std_logic;   
      rst                     : IN std_logic;   
      a                       : IN std_logic_vector(3 DOWNTO 0);   
      d                       : OUT std_logic_vector(6 DOWNTO 0);   
      en                      : OUT std_logic_vector(1 DOWNTO 0));   

END bcd;

ARCHITECTURE arch OF bcd IS


   SIGNAL code_data                :  std_logic_vector(7 DOWNTO 0);   
   SIGNAL c_tmp                    :  std_logic_vector(3 DOWNTO 0);   
   SIGNAL cnt                      :  std_logic_vector(19 DOWNTO 0);   
   SIGNAL en_tmp                    :  std_logic_vector(1 DOWNTO 0);   	
BEGIN

en <= en_tmp;
   PROCESS(clk,rst)
   BEGIN
      
      IF (NOT rst = '1') THEN
         cnt <= "00000000000000000000";    
      ELSIF(clk'event and clk='0')THEN
         IF (cnt /= "01011111111111111111") THEN
            cnt <= cnt + "00000000000000000001";    
         ELSE
            cnt <= "00000000000000000000";    
         END IF;
      END IF;
   END PROCESS;

   PROCESS(clk,rst)
   BEGIN
    
      IF (NOT rst = '1') THEN
         en_tmp <= "01";    
      ELSIF(clk'event and clk='0')THEN
         IF (cnt = "01011111111111111111") THEN
            en_tmp <= NOT en_tmp;    
         END IF;
      END IF;
   END PROCESS;

   PROCESS(en_tmp)
   BEGIN
      CASE en_tmp IS
         WHEN "01" =>
                  c_tmp <= code_data(3 DOWNTO 0);    
         WHEN "10" =>
                  c_tmp <= code_data(7 DOWNTO 4);    
         WHEN OTHERS  =>
                  c_tmp <= "0000";    
         
      END CASE;
   END PROCESS;

   PROCESS(a)
   BEGIN
      CASE a(3 DOWNTO 1) IS
         WHEN "000" =>
                  code_data(7 DOWNTO 1) <= "0000000";    
         WHEN "001" =>
                  code_data(7 DOWNTO 1) <= "0000001";    
         WHEN "010" =>
                  code_data(7 DOWNTO 1) <= "0000010";    
         WHEN "011" =>
                  code_data(7 DOWNTO 1) <= "0000011";    
         WHEN "100" =>
                  code_data(7 DOWNTO 1) <= "0000100";    
         WHEN "101" =>
                  code_data(7 DOWNTO 1) <= "0001000";    
         WHEN "110" =>
                  code_data(7 DOWNTO 1) <= "0001001";    
         WHEN "111" =>
                  code_data(7 DOWNTO 1) <= "0001010";    
         WHEN OTHERS  =>
                  code_data(7 DOWNTO 1) <= "0000000";    
         
      END CASE;
      code_data(0) <= a(0);    
   END PROCESS;

   PROCESS(c_tmp)
   BEGIN
      CASE c_tmp IS
          WHEN "0000"=>d<= "1000000" ; 
          WHEN "0001"=>d<= "1111001" ; 
          WHEN "0010"=>d<= "0100100" ; 
          WHEN "0011"=>d<= "0110000" ; 
          WHEN "0100"=>d<= "0011001" ; 
          WHEN "0101"=>d<= "0010010" ; 
          WHEN "0110"=>d<= "0000010" ;
          WHEN "0111"=>d<= "1111000" ;
          WHEN "1000"=>d<= "0000000" ; 
          WHEN "1001"=>d<= "0010000" ;
          WHEN "1010"=>d<= "0001000" ;
          WHEN "1011"=>d<= "0000011" ;
          WHEN "1100"=>d<= "1000110" ; 
          WHEN "1101"=>d<= "0100001" ;
          WHEN "1110"=>d<= "0000110" ; 
          WHEN "1111"=>d<= "0001110" ;     
           WHEN OTHERS =>
                  NULL;
         
      END CASE;
   END PROCESS;

END arch;
