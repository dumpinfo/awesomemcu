ENTITY  mux21a  IS
PORT (a,b :IN  BIT;
         s:IN  BIT;
         y:OUT BIT);
END ENTITY mux21a ;
ARCHITECTURE  one  OF mux21a IS
  BEGIN
  y<=a when s='0' else  b;
END ARCHITECTURE one;


