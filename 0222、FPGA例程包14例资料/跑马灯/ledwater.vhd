
--跑马灯实验：利用计数器轮流点亮LED灯，实现各种动态效果。
-- 跑马灯实验：利用计数器轮流点亮LED灯，实现各种动态效果。
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY ledwater IS
   PORT (
      clk                     : IN std_logic;   
      rst                     : IN std_logic;   
      dataout                 : OUT std_logic_vector(11 DOWNTO 0));   
END ledwater;

ARCHITECTURE arch OF ledwater IS


   SIGNAL cnt                      :  std_logic_vector(22 DOWNTO 0);   
   SIGNAL dataout_tmp            :  std_logic_vector(11 DOWNTO 0);   

BEGIN
   dataout <= dataout_tmp;

   PROCESS(clk,rst)
   BEGIN
      
      IF (NOT rst = '1') THEN
         cnt <= "00000000000000000000000";    
         dataout_tmp <= "111110011111";   --为0的bit位代表要点亮的LED的位置 
      ELSIF(clk'event and clk='1')THEN
         cnt <= cnt + "00000000000000000000001";    
         IF (cnt = "11111111111111111111111") THEN
            dataout_tmp(4 DOWNTO 0) <= dataout_tmp(5 DOWNTO 1);    
            dataout_tmp(5) <= dataout_tmp(0);    
            dataout_tmp(11 DOWNTO 7) <= dataout_tmp(10 DOWNTO 6);    
            dataout_tmp(6) <= dataout_tmp(11);    
         END IF;
      END IF;
   END PROCESS;

END arch;
