library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY mux IS
   PORT (
      a                       : IN std_logic;   
      b                       : IN std_logic_vector(2 DOWNTO 0);   
      c                       : IN std_logic_vector(2 DOWNTO 0);   
       d                      : OUT std_logic_vector(6 DOWNTO 0);   
      en                      : OUT std_logic_vector(7 DOWNTO 0));   
END mux;

ARCHITECTURE arch OF mux IS


   SIGNAL d_tmp                    :  std_logic_vector(3 DOWNTO 0);   
   SIGNAL temp_xhd               :  std_logic_vector(2 DOWNTO 0);   
BEGIN
   en <= "00000000" ;
   temp_xhd <= b WHEN a = '1' ELSE c;
   d_tmp <= "0" & temp_xhd ;

   PROCESS(d_tmp)
   BEGIN
      CASE d_tmp IS
   WHEN "0000"=>d<= "1000000" ; 
   WHEN "0001"=>d<= "1111001" ; 
   WHEN "0010"=>d<= "0100100" ; 
   WHEN "0011"=>d<= "0110000" ; 
   WHEN "0100"=>d<= "0011001" ; 
   WHEN "0101"=>d<= "0010010" ; 
   WHEN "0110"=>d<= "0000010" ;
   WHEN "0111"=>d<= "1111000" ;
   WHEN "1000"=>d<= "0000000" ; 
   WHEN "1001"=>d<= "0010000" ;
   WHEN "1010"=>d<= "0001000" ;
   WHEN "1011"=>d<= "0000011" ;
   WHEN "1100"=>d<= "1000110" ; 
   WHEN "1101"=>d<= "0100001" ;
   WHEN "1110"=>d<= "0000110" ; 
   WHEN "1111"=>d<= "0001110" ; 
         WHEN OTHERS =>
                  NULL;
         
      END CASE;
   END PROCESS;

END arch;
