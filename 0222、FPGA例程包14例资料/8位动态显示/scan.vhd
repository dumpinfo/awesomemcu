 LIBRARY IEEE ;
 USE IEEE.STD_LOGIC_1164.ALL ;
 USE IEEE.STD_LOGIC_UNSIGNED.ALL ;
 ENTITY scan IS
  PORT ( clk,rst : IN  STD_LOGIC;
         BT : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);  
         SG: OUT STD_LOGIC_VECTOR(6 DOWNTO 0)); 
 END ;
 ARCHITECTURE one OF scan IS
   SIGNAL CNT8 :STD_LOGIC_VECTOR(2 DOWNTO 0);
   SIGNAL  A :INTEGER RANGE 0 TO 15;
   signal  CLK0:std_logic;
   
  constant counter_len:integer:=39999;
begin
  process(clk,rst)
  variable cnt:integer range 0 to counter_len;
  begin
    if(rst='0')then
       cnt:=0;
    elsif clk'event and clk='1' then
       if cnt=counter_len then
          cnt:=0;
       else
          cnt:=cnt+1;
       end if;

       case cnt is
         when 0 to counter_len/2=>CLK0<='0';
         when others            =>CLK0<='1';
       end case;
    end if;
  end process;
P1:PROCESS(CNT8)
BEGIN
CASE  CNT8 IS
WHEN "000"=>BT<="11111110";A<=0;
WHEN "001"=>BT<="11111101";A<=1;
WHEN "010"=>BT<="11111011";A<=2;
WHEN "011"=>BT<="11110111";A<=3;
WHEN "100"=>BT<="11101111";A<=4;
WHEN "101"=>BT<="11011111";A<=5;
WHEN "110"=>BT<="10111111";A<=6;
WHEN "111"=>BT<="01111111";A<=7;
WHEN OTHERS =>  NULL ;
END CASE;
END  PROCESS P1;

P2:PROCESS(CLK0)
   
BEGIN

IF CLK0'EVENT AND CLK0='1'THEN CNT8<=CNT8+1;
END IF;
END PROCESS P2; 

 P3:PROCESS( A )
  BEGIN
  CASE  A  IS
   WHEN 0 =>SG<= "1000000" ; 
   WHEN 1 =>SG<= "1111001" ; 
   WHEN 2 =>SG<= "0100100" ; 
   WHEN 3 =>SG<= "0110000" ; 
   WHEN 4 =>SG<= "0011001" ; 
   WHEN 5 =>SG<= "0010010" ; 
   WHEN 6=> SG <="0000010" ;
   WHEN 7 =>SG <="1111000" ;
   WHEN 8 =>SG<= "0000000" ; 
   WHEN 9 =>SG<= "0010000" ;
   WHEN 10 =>SG<= "0001000" ;
   WHEN 11 =>SG<= "0000011" ;
   WHEN 12 =>SG<= "1000110" ; 
   WHEN 13 =>SG <= "0100001" ;
   WHEN 14 =>SG<= "0000110" ; 
   WHEN 15 =>SG <= "0001110" ;
   WHEN OTHERS =>  NULL ;
   END CASE ;
  END PROCESS P3;
 END ;