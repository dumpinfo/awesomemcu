library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
ENTITY encode IS
   PORT (
      a                       : IN std_logic_vector(7 DOWNTO 0);   
      c                       : OUT std_logic_vector(6 DOWNTO 0);   
      en                      : OUT std_logic_vector(7 DOWNTO 0));   
END encode;

ARCHITECTURE arch OF encode IS


   SIGNAL c_tmp :  std_logic_vector(3 DOWNTO 0);   
 

BEGIN
   en<= "00000000" ;

   PROCESS(a) 
   BEGIN	
 		if(a(7)='1') then c_tmp<="1000";
		elsif(a(6)='1') then c_tmp<="0111";
		elsif(a(5)='1') then c_tmp<="0110";
		elsif(a(4)='1') then c_tmp<="0101";
		elsif(a(3)='1') then c_tmp<="0100";
		elsif(a(2)='1') then c_tmp<="0011";
		elsif(a(1)='1') then c_tmp<="0010";
		elsif(a(0)='1') then c_tmp<="0001";
		else c_tmp<="0000";
		end if;
   END PROCESS;
		
   PROCESS(c_tmp)
   BEGIN
      CASE c_tmp IS
          WHEN "0000"=> c<= "1000000" ; 
          WHEN "0001"=>c<= "1111001" ; 
          WHEN "0010"=>c<= "0100100" ; 
          WHEN "0011"=>c<= "0110000" ; 
          WHEN "0100"=>c<= "0011001" ; 
          WHEN "0101"=>c<= "0010010" ; 
          WHEN "0110"=>c<= "0000010" ;
          WHEN "0111"=>c<= "1111000" ;
          WHEN "1000"=>c<= "0000000" ; 
          WHEN "1001"=>c<= "0010000" ;
          WHEN "1010"=>c<= "0001000" ;
          WHEN "1011"=>c<= "0000011" ;
          WHEN "1100"=>c<= "1000110" ; 
          WHEN "1101"=>c<= "0100001" ;
          WHEN "1110"=>c<= "0000110" ; 
          WHEN "1111"=>c<= "0001110" ;
         WHEN OTHERS =>
                  NULL;
         
      END CASE;
   END PROCESS;

END arch;
