-------------------------------------------------

--实体名：fen1
--功  能：对输入时钟进行40000000分频，得到1Hz信号
--接  口：clk -时钟输入
--        qout-秒输出信号

-------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity fen1 is
port
(clk:in std_logic;
 rst:in std_logic;
 qout:out std_logic
);
end fen1;

architecture behave of fen1 is
constant counter_len:integer:=39999999;
begin
  process(clk,rst)
  variable cnt:integer range 0 to counter_len;
  begin
    if(rst='0')then
       cnt:=0;
    elsif clk'event and clk='1' then
       if cnt=counter_len then
          cnt:=0;
       else
          cnt:=cnt+1;
       end if;

       case cnt is
         when 0 to counter_len/2=>qout<='0';
         when others            =>qout<='1';
       end case;
    end if;
  end process;

end behave;