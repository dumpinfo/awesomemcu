-------------------------------------------------

--实体名：decode47
--功  能：实现数码显示管的编码显示
--接  口：qin -BCD码输入
--        qout-七段码输出

-------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity decode47 is
port
(qin  : in  std_logic_vector(3 downto 0);
 qout : out std_logic_vector(6 downto 0)
);
end decode47;

architecture behave of decode47 is
begin
  with qin select
    qout<="0111111" when "0000",  --显示0
          "0000110" when "0001",  --显示1
          "1011011" when "0010",  --显示2
          "1001111" when "0011",  --显示3
          "1100110" when "0100",  --显示4
          "1101101" when "0101",  --显示5 
          "1111101" when "0110",  --显示6
          "0000111" when "0111",  --显示7
          "1111111" when "1000",  --显示8
          "1101111" when "1001",  --显示9
          "1110111" when "1010",  --显示A
          "1111100" when "1011",  --显示B
          "0111001" when "1100",  --显示C
          "1011110"  when "1101",  --显示D
          "1111001" when "1110",  --显示E
          "1110001"  when "1111",  --显示-
          "1111111" when others;
 end behave;







 
  
  
 
  
  

  
 
  
  
  
