--
-- 作为一个简单示例，本实验实现一个能显示小时，分钟，秒的数字时钟。
-- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY clock IS
   PORT (
      clk                     : IN std_logic;   
      rst                     : IN std_logic;   
      dataout                 : OUT std_logic_vector(7 DOWNTO 0);   
      en                      : OUT std_logic_vector(7 DOWNTO 0));   
END clock;

ARCHITECTURE arch OF clock IS
 SIGNAL div_cnt                  :  std_logic_vector(18 DOWNTO 0); --分频计数器  
 SIGNAL data4                    :  std_logic_vector(3 DOWNTO 0);    
 SIGNAL en_xhdl                   :  std_logic_vector(7 DOWNTO 0);
 SIGNAL dataout_xhdl1            :  std_logic_vector(7 downto 0);

 SIGNAL s1_cnt                   :  std_logic_vector(3 DOWNTO 0); ---秒的个位计数器 逢9进
 SIGNAL s2_cnt                   :  std_logic_vector(2 DOWNTO 0); ---秒的十位计数器 逢6进
 SIGNAL m1_cnt                   :  std_logic_vector(3 DOWNTO 0); ---分的个位计数器 逢9进
 SIGNAL m2_cnt                   :  std_logic_vector(2 DOWNTO 0); ---分的十位计数器 逢6进
 SIGNAL h1_cnt                   :  std_logic_vector(3 DOWNTO 0); ---小时的个位计数器 逢9进
 SIGNAL h2_cnt                   :  std_logic_vector(1 DOWNTO 0); ---小时的十位计数器 逢2进
 SIGNAL s1_over                  :  std_logic;                    ---秒的个位计数器溢出标志
 SIGNAL S2_over                  :  std_logic;                    ---秒的十位计数器溢出标志
 SIGNAL m1_over                  :  std_logic;                    ---分的个位计数器溢出标志
 SIGNAL m2_over                  :  std_logic;                    ---分的个位计数器溢出标志
 SIGNAL h1_over                  :  std_logic;                    ---小时的个位计数器溢出标志

 SIGNAL timer_over               :  std_logic;                    ---时钟完成24小时运转

 constant dot      : std_logic_vector(3 downto 0) :="1010";
begin


  PROCESS(clk,rst)
   BEGIN
 
      IF (NOT rst = '1') THEN
         div_cnt <= "000000000000000000000";             
      ELSIF(clk'EVENT AND clk = '1')THEN
         if(div_cnt="1100001101010000000")then
            div_cnt<="0000000000000000000";
         else
         div_cnt <= div_cnt + 1;        
      END IF;
   END PROCESS;

process(div_cnt(20),rst)                 ---秒的个位计数器 逢9进
begin
 if(rst='0')then
   s1_cnt<="0000";
   s1_over<='0';
 elsif(div_cnt(20)'event and div_cnt(20)='1')then
   if(s1_cnt="1001" )then
      s1_cnt<="0000";
      s1_over<='1';
   else
      s1_over<='0';
      s1_cnt<=s1_cnt+1;
   end if;
  end if;
end process;

process(rst,s1_over)                 ---秒的十位计数器 逢5进
begin
 if(rst='0')then
   s2_cnt<="000";
   s2_over<='0';
 elsif(s1_over'event and s1_over='1')then
   if(s2_cnt="101" )then
      s2_cnt<="000";
      s2_over<='1';
   else
      s2_over<='0';
      s2_cnt<=s2_cnt+1;
   end if;
  end if;
end process;

process(rst,s2_over)                 ---分的个位计数器 逢10进
begin
 if(rst='0')then
   m1_cnt<="0000";
   m1_over<='0';
 elsif(s2_over'event and s2_over='1')then
   if(m1_cnt="1001" )then
      m1_cnt<="0000";
      m1_over<='1';
   else
      m1_over<='0';
      m1_cnt<=m1_cnt+1;
   end if;
  end if;
end process;

process(rst,m1_over)                 ---分的十位计数器 逢5进
begin
 if(rst='0')then
   m2_cnt<="000";
   m2_over<='0';
 elsif(m1_over'event and m1_over='1')then
   if(m2_cnt="101" )then
      m2_cnt<="000";
      m2_over<='1';
   else
      m2_over<='0';
      m2_cnt<=m2_cnt+1;
   end if;
  end if;
end process;

process(rst,m2_over,timer_over)                 ---小时的个位计数器 逢9进
begin
 if(rst='0')then
   h1_cnt<="0010";
   h1_over<='0';
 elsif(m2_over'event and m2_over='1')then
   if(h1_cnt="1001" or timer_over='1')then
      h1_cnt<="0000";
      h1_over<='1';
   else
      h1_over<='0';
      h1_cnt<=h1_cnt+1;
   end if;
  end if;
end process;

process(rst,h1_over)                 ---小时的十位计数器 逢2进
begin
 if(rst='0')then
   h2_cnt<="01";
 elsif(h1_over'event and h1_over='1')then
   if(h2_cnt="10" and timer_over='1')then
      h2_cnt<="00";
   else
      h2_cnt<=h2_cnt+1;
   end if;
  end if;
end process;

timer_over<='1' when (h1_cnt="0100" and h2_cnt<="10" ) else 
        '0';

---*******************显示部分***********************--

en<=en_xhdl;
dataout<=dataout_xhdl1;

process(clk,rst,div_cnt(15 downto 13))
 begin
 if(rst='0')then
 en_xhdl<="11111110";
elsif(clk'event and clk='1')then
 case div_cnt(19 downto 17) is
     when"000"=> en_xhdl<="11111110";
     when"001"=> en_xhdl<="11111101";
     when"010"=> en_xhdl<="11111011";
     when"011"=> en_xhdl<="11110111";
     when"100"=> en_xhdl<="11101111";
     when"101"=> en_xhdl<="11011111";
     when"110"=> en_xhdl<="10111111";
     when"111"=> en_xhdl<="01111111";
     when others=> en_xhdl<="11111110";
  end case;
end if;

 end process;

process(clk,rst,en_xhdl,s1_cnt,s2_cnt,m1_cnt,m2_cnt,h1_cnt,h2_cnt)
begin
if(rst='0')then
   data4<=dot;
elsif(clk'event and clk='1')then
case en_xhdl is
   when "11111110"=> data4<=s1_cnt;
   when "11111101"=> data4<='0'&s2_cnt;
   when "11111011"=> data4<=dot;
   when "11110111"=> data4<=m1_cnt;
   when "11101111"=> data4<='0'&m2_cnt;
   when "11011111"=> data4<=dot;
   when "10111111"=> data4<=h1_cnt;
   when "01111111"=> data4<="00"&h2_cnt;
   when others => data4<=dot;
  end case;
end if;
end process;

process(data4)
begin
  case data4 is
         WHEN "0000" =>
                  dataout_xhdl1 <= "00000011";    
         WHEN "0001" =>
                  dataout_xhdl1 <= "10011111";    
         WHEN "0010" =>
                  dataout_xhdl1 <= "00100101";    
         WHEN "0011" =>
                  dataout_xhdl1 <= "00001101";    
         WHEN "0100" =>
                  dataout_xhdl1 <= "10011001";    
         WHEN "0101" =>
                  dataout_xhdl1 <= "01001001";    
         WHEN "0110" =>
                  dataout_xhdl1 <= "01000001";    
         WHEN "0111" =>
                  dataout_xhdl1 <= "00011111";    
         WHEN "1000" =>
                  dataout_xhdl1 <= "00000001";    
         WHEN "1001" =>
                  dataout_xhdl1 <= "00011001";    
         WHEN "1010" =>
                  dataout_xhdl1 <= "11111101";    
         WHEN "1011" =>
                  dataout_xhdl1 <= "11000001";    
         WHEN "1100" =>
                  dataout_xhdl1 <= "01100011";    
         WHEN "1101" =>
                  dataout_xhdl1 <= "10000101";    
         WHEN "1110" =>
                  dataout_xhdl1 <= "01100001";    
         WHEN "1111" =>
                  dataout_xhdl1 <= "01110001";    
         WHEN OTHERS =>
                  dataout_xhdl1 <= "00000011"; 
         
      END CASE;
   END PROCESS;
end arch;











 
