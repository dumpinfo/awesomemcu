-------------------------------------------------
--实体名：sel
--功  能：实现六个数码显示管扫描显示
--接  口：clk -时钟输入
--        qin1-第一个数码显示管要显示内容输入
--        qin2-第二个数码显示管要显示内容输入
--        qin3-第三个数码显示管要显示内容输入
--        qin4-第四个数码显示管要显示内容输入
--        qin5-第五个数码显示管要显示内容输入
--        qin6-第六个数码显示管要显示内容输入
--        sel -位选信号输出

-------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity sel is
port
(clk  : in  std_logic;
 rst  : in  std_logic;
 qin1 : in  std_logic_vector(3 downto 0);
 qin2 : in  std_logic_vector(3 downto 0);
 qin3 : in  std_logic_vector(3 downto 0);
 qin4 : in  std_logic_vector(3 downto 0);
 qin5 : in  std_logic_vector(3 downto 0);
 qin6 : in  std_logic_vector(3 downto 0);
 qout : out std_logic_vector(3 downto 0);
 sel  : out std_logic_vector(5 downto 0)
);
end sel;

architecture behave of sel is
begin
  process(clk,rst)
  variable cnt:integer range 0 to 5;
  begin
    if(rst='0')then
       cnt:=0;
       sel<="111110";
       qout<="0000";
    elsif clk'event and clk='1' then
       if cnt=5 then
          cnt:=0;
       else
          cnt:=cnt+1;
       end if;      
       case cnt is
         when 0=>qout<=qin1;
                 sel <="111110";
         when 1=>qout<=qin2;
                 sel <="111101";
         when 2=>qout<=qin3;
                 sel <="111011";
         when 3=>qout<=qin4;
                 sel <="110111";
         when 4=>qout<=qin5;
                 sel <="101111";
         when 5=>qout<=qin6;
                 sel <="011111";
         
         when others=>qout<="0000";
                 sel <="111111";
       end case;
     end if;
  end process;
end behave; 
                 