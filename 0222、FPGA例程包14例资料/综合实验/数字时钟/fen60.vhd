-------------------------------------------------

--功  能：60进制计数器
--接  口：clk  -时钟输入
--        qout1-个位BCD输出
--        qout2-十位BCD输出
--        carry-进位信号输出

-------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity fen60 is 
port
(clk   : in  std_logic;
 rst   : in  std_logic;
 qout1 : out std_logic_vector(3 downto 0);
 qout2 : out std_logic_vector(3 downto 0);
 carry : out std_logic
);
end fen60;

architecture behave of fen60 is
signal tem1:std_logic_vector(3 downto 0);
signal tem2:std_logic_vector(3 downto 0);
begin
  process(clk,rst)
  begin
    if(rst='0')then
      tem1<="0000";
      tem2<="0000";
    elsif clk'event and clk='1' then 
       if tem1="1001" then
          tem1<="0000";
          if tem2="0101" then
             tem2<="0000";
             carry<='1';
          else
             tem2<=tem2+1;
             carry<='0';
          end if;
       else
          tem1<=tem1+1;
       end if;
    end if;
    qout1<=tem1;
    qout2<=tem2;
  end process;
end behave;  