--
-- 本实验模拟路口的红黄绿交通灯的变化过程，用LED灯表示交通灯，并在数码管上显示当前状态剩余时间。
-- 红灯持续时间为30秒，黄灯3秒，绿灯30秒
-- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY traffic IS
   PORT (
      clk                     : IN std_logic;   
      rst                     : IN std_logic;   
      dataout                 : OUT std_logic_vector(7 DOWNTO 0);  --数码管段数据 
      en                      : OUT std_logic_vector(1 DOWNTO 0);  --数码管使能
      lightY                  : OUT std_logic_vector(3 downto 0);  --黄灯
      lightG                  : OUT std_logic_vector(3 downto 0);  --绿灯
      lightR                  : OUT std_logic_vector(3 DOWNTO 0)); --红灯  
END traffic;

ARCHITECTURE arch OF traffic IS
   SIGNAL div_cnt  : std_logic_vector(24 downto 0);
   SIGNAL data4              :  std_logic_vector(3 DOWNTO 0); 
  
   SIGNAL first                    :  std_logic_vector(3 DOWNTO 0); --时间的个位  
   SIGNAL second                   :  std_logic_vector(3 DOWNTO 0); --时间的十位  
   SIGNAL state                    :  std_logic_vector(1 DOWNTO 0);   
   CONSTANT  red                   :  std_logic_vector(1 DOWNTO 0) := "00";    
   CONSTANT  yellow                :  std_logic_vector(1 DOWNTO 0) := "01";    
   CONSTANT  green                 :  std_logic_vector(1 DOWNTO 0) := "10";    
   SIGNAL dataout_xhdl1            :  std_logic_vector(7 DOWNTO 0);   
   SIGNAL en_xhdl                  :  std_logic_vector(1 DOWNTO 0);   
   SIGNAL light_xhdl3              :  std_logic_vector(11 DOWNTO 0);   
   
BEGIN


   PROCESS(clk,rst)
   BEGIN
      IF (NOT rst = '1') THEN
         div_cnt <= "0000000000000000000000000";    
      ELSIF(clk'EVENT AND clk = '1')THEN
         div_cnt <= div_cnt + 1;    
      END IF;
   END PROCESS;

lightG<=light_xhdl3(3 downto 0);
lightY<=light_xhdl3(7 downto 4);
lightR<=light_xhdl3(11 downto 8);


PROCESS(div_cnt(20),rst)
   BEGIN
      
      IF (NOT rst = '1') THEN
         state <= red;    
         first <= "0000";    
         second <= "0011";    
      ELSIF(div_cnt(24)'EVENT AND div_cnt(24) = '1')THEN
             CASE state IS
               WHEN red =>
                        IF (first /= "0000") THEN
                           first <= first - "0001";    
                        ELSE
                           IF (second /= "0000") THEN
                              second <= second - "0001";    
                              first <= "1001";    
                           ELSE
                              state <= green;    
                              second <= "0011";    
                           END IF;
                        END IF;
               WHEN green =>
                        IF (first /= "0000") THEN
                           first <= first - "0001";    
                        ELSE
                           IF (second /= "0000") THEN
                              first <= "1001";    
                              second <= second - "0001";    
                           ELSE
                              state <= yellow;    
                              second <= "0000";
                              first <= "0101";    
                           END IF;
                        END IF;
               WHEN yellow =>
                        IF (first /= "0000" OR second /= "0000") THEN
                           IF (second /= "0000") THEN
                              first <= "1001";    
                              second <= second - "0001";    
                           ELSE
                              first <= first - "0001";    
                           END IF;
                        ELSE
                           state <= red;    
                           second <= "0011";    
                        END IF;
               WHEN OTHERS  =>
                        first <= "0000";    
                        second <= "0000";    
               
            END CASE;
         END IF;

   END PROCESS;

   PROCESS(state)
   BEGIN
      CASE state IS
         WHEN red =>
                  light_xhdl3 <= "000011111111";    
         WHEN yellow =>
                  light_xhdl3 <= "111100001111";    
         WHEN green =>
                  light_xhdl3 <= "111111110000";    
         WHEN OTHERS  =>
                  light_xhdl3 <= "111111111111";    
         
      END CASE;
   END PROCESS;

---*********显示部分**********---


en<=en_xhdl;
dataout<=dataout_xhdl1;

process(div_cnt(15),rst)
 begin
   if(rst='0')then
   en_xhdl<="10";
   elsif(div_cnt(15)'event and div_cnt(15)='1')then
   en_xhdl(1)<= not en_xhdl(1);
   en_xhdl(0)<= not en_xhdl(0);
   end if;
end process;

process(en_xhdl,data4,first,second)
begin
 if(en_xhdl="10")then
   data4<=first;
 else
   data4<=second;
end if;
end process;

process(data4)
begin
  case data4 is
   WHEN "0000" =>
                  dataout_xhdl1 <= "00000011";    
         WHEN "0001" =>
                  dataout_xhdl1 <= "10011111";    
         WHEN "0010" =>
                  dataout_xhdl1 <= "00100101";    
         WHEN "0011" =>
                  dataout_xhdl1 <= "00001101";    
         WHEN "0100" =>
                  dataout_xhdl1 <= "10011001";    
         WHEN "0101" =>
                  dataout_xhdl1 <= "01001001";    
         WHEN "0110" =>
                  dataout_xhdl1 <= "01000001";    
         WHEN "0111" =>
                  dataout_xhdl1 <= "00011111";    
         WHEN "1000" =>
                  dataout_xhdl1 <= "00000001";    
         WHEN "1001" =>
                  dataout_xhdl1 <= "00011001";    
         WHEN "1010" =>
                  dataout_xhdl1 <= "00010001";    
         WHEN "1011" =>
                  dataout_xhdl1 <= "11000001";    
         WHEN "1100" =>
                  dataout_xhdl1 <= "01100011";    
         WHEN "1101" =>
                  dataout_xhdl1 <= "10000101";    
         WHEN "1110" =>
                  dataout_xhdl1 <= "01100001";    
         WHEN "1111" =>
                  dataout_xhdl1 <= "01110001";    
         WHEN OTHERS =>
                  NULL;
         
      END CASE;
   END PROCESS;
   
end arch;