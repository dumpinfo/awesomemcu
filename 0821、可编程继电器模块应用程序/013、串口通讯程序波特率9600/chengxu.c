/********************************************************************
                            汇诚科技

实现功能:按键触发延时1分钟程序
使用芯片：STC15F104E
晶振：12MHZ
波特率：9600
编译环境：Keil
作者：zhangxinchunleo
网站：www.ourhc.cn
淘宝店：汇诚科技 http://shop36330473.taobao.com
【声明】此程序仅用于学习与参考，引用请注明版权和作者信息！     

*********************************************************************/
/********************************************************************/

#include<reg52.h>  	       //库文件
#define uchar unsigned char//宏定义无符号字符型
#define uint unsigned int  //宏定义无符号整型
/********************************************************************
                            初始定义
*********************************************************************/
uchar dat; //用于存储单片机接收发送缓冲寄存器SBUF里面的内容
/********************************************************************
                            I/O定义
*********************************************************************/
sbit P33=P3^3;	 //定义单片机P3口的第3位 （即P3.3）
sbit P3_5=P3^5;
/********************************************************************
                            延时函数
*********************************************************************/
void delay(uchar t)
{
  uchar i,j;
   for(i=0;i<t;i++)
   {
   	 for(j=13;j>0;j--);
	 { ;
	 }
   }
}


/********************************************************************
                    串口初始化,波特率9600
*********************************************************************/
void UartInit(void)		//9600bps@11.0592MHz
{
	SCON = 0x50;		//8位数据,可变波特率
	AUXR |= 0x40;		//定时器1时钟为Fosc,即1T
	AUXR &= 0xFE;		//串口1选择定时器1为波特率发生器
	TMOD &= 0x0F;		//设定定时器1为16位自动重装方式
	TL1 = 0xE0;		//设定定时初值
	TH1 = 0xFE;		//设定定时初值
	ET1 = 0;		//禁止定时器1中断
	TR1 = 1;		//启动定时器1
}

/********************************************************************
                            主函数
*********************************************************************/
void main()
{
Init_Com();//串口初始化
while(1)
{
                      
if ( RI ) //扫描判断是否接收到数据，
{
dat = SBUF; //接收数据SBUF赋与dat
RI=0; //RI 清零。
SBUF = dat; //在原样把数据发送回去
} 
}
}
	
/********************************************************************
                              结束
*********************************************************************/
